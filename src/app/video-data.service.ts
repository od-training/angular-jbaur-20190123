import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Video } from './types';

const videoUrl = 'https://api.angularbootcamp.com/videos';

@Injectable({
  providedIn: 'root'
})
export class VideoDataService {

  constructor(private http: HttpClient) { }

  loadVideos(): Observable<Video[]> {
    return this.http.get<Video[]>(videoUrl).pipe(
      map(videos => {
        videos.map(video => video.author = 'John Baur');
        return videos;
      })
    );
  }

  loadVideo(id: string): Observable<Video> {
    return this.http.get<Video>(`${videoUrl}/${id}`);
  }
}
