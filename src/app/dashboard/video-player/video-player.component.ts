import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map, switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { VideoDataService } from 'src/app/video-data.service';
import { Video } from 'src/app/types';

@Component({
  selector: 'nyc-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.css']
})
export class VideoPlayerComponent implements OnInit {
  video: Observable<Video>;

  constructor(route: ActivatedRoute, vs: VideoDataService) {
    this.video = route.queryParams.pipe(
      map(params => params['video']),
      switchMap(id => vs.loadVideo(id))
    );
  }

  ngOnInit() {
  }

}
