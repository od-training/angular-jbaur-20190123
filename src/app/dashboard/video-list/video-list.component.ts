import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { pluck, tap } from 'rxjs/operators';

import { Video } from '../../types';

@Component({
  selector: 'nyc-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.css']
})
export class VideoListComponent implements OnInit {
  @Input() videos: Video[];
  selectedVideoId: Observable<string>;

  constructor(route: ActivatedRoute) {
    this.selectedVideoId = route.queryParams.pipe(
      pluck('video')
    );
  }

  ngOnInit() {
  }

}
