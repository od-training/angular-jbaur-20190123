import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { Video } from 'src/app/types';
import { VideoDataService } from 'src/app/video-data.service';

@Component({
  selector: 'nyc-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.css']
})
export class VideoDashboardComponent implements OnInit {
  dashboardVideos: Observable<Video[]>;

  constructor(videoService: VideoDataService) {
    this.dashboardVideos = videoService.loadVideos();
  }

  ngOnInit() {
  }

}
